const testArray = ['hello', 'world', 23, '1453', null, true, false, 68];

function filterBy(array, dataType) {
    const filteredArray = [];
    array.forEach((element => {
        if(typeof element !== dataType){
            filteredArray.push([element]);
        }
    }));
    return filteredArray;
}

console.log(`Not a strings: ${filterBy(testArray, `string`)}`);
console.log(`Not a numbers: ${filterBy(testArray, `number`)}`);
console.log(`Not a boolean: ${filterBy(testArray, `boolean`)}`);

